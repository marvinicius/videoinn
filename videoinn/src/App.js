import React from 'react';
import MenuPrincipal from "./components/MenuPrincipal";
import MenuSuspenso from "./components/MenuSuspenso";

const app = () => (
  <div>
    <MenuPrincipal />
    <MenuSuspenso />
  </div>
);

export default app;
