import React from 'react';
import ReactDOM from 'react-dom';
import Login from "./pages/Login";
import Home from "./pages/Home";
import Listagem from "./pages/Lista";
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route } from "../node_modules/react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(
    (<Router>
        <div>
            <Route exact path="/" component={Login}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/lista" component={Listagem}/>
        </div>
    </Router>), 
    document.getElementById('root')
);

registerServiceWorker();