import React from 'react';
import PubSub from "pubsub-js";
import Drawer from 'material-ui/Drawer';
import {List, ListItem} from 'material-ui/List';
import GroupIcon from "material-ui/svg-icons/social/group";
import VideoIcon from "material-ui/svg-icons/av/movie";

export default class MenuSuspenso extends React.Component {

    constructor(props) {
      super(props);
      this.state = {open: false};
    }
  
    handleToggle = () => this.setState({open: !this.state.open});
  
    handleClose = () => this.setState({open: false});
  
    componentDidMount() {
      PubSub.subscribe("abrir-menu-suspenso", this.handleToggle);
    }

    render() {

      return (
        <div>
          <Drawer
            docked={false}
            width={200}
            open={this.state.open}
            onRequestChange={(open) => this.setState({open})}
          >
            <List>
              <ListItem
                primaryText="Playlists"
                leftIcon={<GroupIcon />}
                initiallyOpen={false}
                primaryTogglesNestedList={true}
                nestedItems={[
                  <ListItem
                    key={1}
                    primaryText="Lista 1"
                    leftIcon={<VideoIcon />}
                    href="/lista"
                  />
                ]}
              />
            </List>
          </Drawer>
        </div>
      );
    };
    
};
  







