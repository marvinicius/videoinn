import React from 'react';
import AppBar from 'material-ui/AppBar';
import PubSub from "pubsub-js";
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {white} from 'material-ui/styles/colors';

const style = {
  backgroundColor: "#17a2b8"
};

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon color={white}/></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Home" href="/home" />
  </IconMenu>
);

export default class MenuPrincipal extends React.Component {
  state = {
    logged: true,
  };

  openMenuSuspenso() {
    PubSub.publish("abrir-menu-suspenso");
  }

  render() {
    return (
      <div>
        <AppBar
          title="VideoInn"
          style={style}
          onLeftIconButtonClick={this.openMenuSuspenso}
          iconElementRight={this.state.logged ? <Logged /> : <Logged />}
        />
      </div>
    );
  }
}
