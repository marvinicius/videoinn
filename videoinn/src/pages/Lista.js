import React from 'react';
import App from "../App"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import YouTube from 'react-youtube';

export default class Listagem extends React.Component{
   
      _onReady(event) {
        event.target.loadVideoById("1FJD7jZqZEk");
        event.target.mute()
      }
    
      _onEnd(event){
        event.target.stopVideo();
        console.log(event.target.getVideoUrl());
        
        var video1="https://www.youtube.com/watch?t=150&v=1FJD7jZqZEk";
        var video2="https://www.youtube.com/watch?t=93&v=u3wtVI-aJuw";
        var video3="https://www.youtube.com/watch?t=98&v=8qNxyP6u2Kg";
        var video4="https://www.youtube.com/watch?t=47&v=La3fyRYp8ts";
        var video5="https://www.youtube.com/watch?t=143&v=4jGRyEa2jhE";
      
        
        if(video1 === event.target.getVideoUrl()){
          this.perguntaVideo1(event);
          }else if(video2 === event.target.getVideoUrl()){
            this.perguntaVideo2(event);
            }else if(video3 === event.target.getVideoUrl()){
              this.perguntaVideo3(event);    
              }else if(video4 === event.target.getVideoUrl()){
                this.perguntaVideo4(event);    
                }else if(video5 === event.target.getVideoUrl()){
                  this.perguntaVideo5(event);    
                }
              }
    
      perguntaVideo1(event){
        
        var person = prompt("É correto afirmar que o conteudo do filme é sobre dinossauros? digite sim ou não:", "" );
        var resposta;

        if (person === null || person === "") {
          alert ("você cancelou")
          } else if(person==="sim"){
              alert("voce digitou " + person + ", resposta certa!")
            }else{
              alert("voce digitou " + person + ", resposta errada, o video será repetido")
            }

        resposta = person;

        if(resposta==="sim"){
          event.target.loadVideoById("u3wtVI-aJuw");
          event.target.playVideo();
          }else{
              event.target.playVideo();
              }
          }  
    
      perguntaVideo2(event){
        var person = prompt("É correto afirmar que esse filme é de carros ? responda sim ou não: ", "" );
        var resposta;
        

          if (person === null || person === "") {
            alert ("você cancelou")
            } else if(person==="sim"){
                alert("voce digitou " + person + ", resposta certa!")
              }else{
                alert("voce digitou " + person + ", resposta errada, o video será repetido")
              }

          resposta = person;

        if(resposta==="sim"){
          event.target.loadVideoById("8qNxyP6u2Kg");
          event.target.playVideo();
              }else{
                event.target.playVideo();
                }
          }

      perguntaVideo3(event){
        var person = prompt("É correto afirmar que o conteúdo do video é de novela? responda: sim ou não: ", "" );
        var resposta;

    
          if (person === null || person === "") {
            alert ("você cancelou")
            } else if(person==="nao" || person==="não"){
                alert("voce digitou " + person + ", resposta certa!")
              }else{
                alert("voce digitou " + person + ", resposta errada, o video será repetido")
              }
    
          resposta = person;
    
        if(resposta==="nao" || resposta==="não"){
          event.target.loadVideoById("La3fyRYp8ts");
          event.target.playVideo();
            }else{
              event.target.playVideo();
          }
        }

      perguntaVideo4(event){
        var person = prompt("É correto afirmar que o conteúdo do video é de um anime? responda: sim ou não: ", "" );
        var resposta;
    
          if (person === null || person === "") {
            alert ("você cancelou")
            } else if(person==="sim"){
                alert("voce digitou " + person + ", resposta certa!")
              }else{
                alert("voce digitou " + person + ", resposta errada, o video será repetido")
              }
      
          resposta = person;
    
        if(resposta==="sim"){
          event.target.loadVideoById("4jGRyEa2jhE");
          event.target.playVideo();
            }else{
              event.target.playVideo();
          }
        }

      perguntaVideo5(event){
        var person = prompt("É correto afirmar que o conteúdo do video é de uma banda de rock? responda: sim ou não: ", "" );
        var resposta;
      
          if (person === null || person === "") {
            alert ("você cancelou")
             } else if(person==="nao" || person === "não"){
                alert("voce digitou " + person + ", resposta certa!")
             }else{
                alert("voce digitou " + person + ", resposta errada, o video será repetido")
              }
      
            resposta = person;
      
          if(resposta==="nao" || resposta==="não"){

            event.target.destroy();
            alert("Parabens! \nVocê respondeu corretamente a todas as perguntas.");

              }else{
                event.target.playVideo();
            }
          }


          render(){
            const styles = {
              root: {
                marginTop: "8%"
              },
              text: {
                color: "primary"
              }
            };

            const opts = {
              height: '520',
              width: '820',
              playerVars: { // https://developers.google.com/youtube/player_parameters
                autoplay: 1,
              }
            };

          return(
              <div> 
                
                <div>
                  <MuiThemeProvider>
                      <App />
                      </MuiThemeProvider>
                      <div style={styles.root}>
                          <div align="center">
                          <YouTube
                            opts={opts}
                            onReady={this._onReady}
                            onEnd={this._onEnd}
                            perguntaVideo1={this.perguntaVideo1}
                            perguntaVideo2={this.perguntaVideo2}
                            perguntaVideo3={this.perguntaVideo3}
                            perguntaVideo4={this.perguntaVideo4}
                            perguntaVideo5={this.perguntaVideo5}
                          />
                          </div>
                      </div>            
                </div>
              </div>
              
          );
        }  
      }
 