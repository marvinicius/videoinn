import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import { Container, Row, Col, Form, Input, Button  } from 'reactstrap';
import { Link } from "react-router-dom";


const style = {
    backgroundColor: "#17a2b8",
    root:{
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: "0.5%",
    marginRight: "0.5%",
    marginTop: "3%"
},
logo:{
    marginTop: "5%",
}
};

const login = () => (
    <MuiThemeProvider>
        <div>
            <AppBar
                style={style}        
            />
            <div style={style.logo}>
                    <fieldset>
                    <h1 align="center">VideoInn</h1>
                        </fieldset>
                        </div>
            <div style={style.root}> 
            <Container>
                <Row>
                    <Col md={{ size: 4, offset: 4 }}>
                        <div>
                            <div>
                                <Form>
                                    <fieldset>
                                        <div className="form-group">
                                            <Input className="form-control" placeholder="Usuário" name="user" type="user"/>
                                        </div>
                                        <div className="form-group">
                                            <Input className="form-control" placeholder="Senha" name="password" type="password" value=""/>
                                        </div>
                                        <div align="center">
                                        <Link to={{pathname:"/home"}}><Button color="info" size="lg">Login</Button></Link>
                                        </div>
                                    </fieldset>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
            </div>
        </div>
    </MuiThemeProvider>
);

export default login;
