import React from 'react';
import App from "../App";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Container, Row, Col, Form, Label, Button, ButtonGroup } from 'reactstrap';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: "0.5%",
    marginRight: "0.5%",
    marginTop: "20%"
  },
  button: {
      marginTop: "5%"
  }
};

const home = () => (
  <MuiThemeProvider>
    <div>
      <App />
      <div style={styles.root}>
      <Container>
                <Row>
                    <Col>
                        <div>
                            <div>
                                <Form>
                                    <fieldset>
                                        <div className="form-group" align="center">
                                            <h2>
                                                <Label>Bem vindo de volta,&nbsp;</Label>
                                                <Label id="userName">Usuario!</Label>
                                            </h2>
                                            <div style={styles.button}>
                                                <ButtonGroup>
                                                <Button color="info" size="lg" href="/lista">Acesse a playlist</Button>
                                                </ButtonGroup>
                                            </div>
                                        </div>
                                    </fieldset>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
      </div> 
    </div>
  </MuiThemeProvider>
);



export default home;